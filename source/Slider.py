# Expression Controller boot file
# varshaninbox
#
# Requires CircuitPython and MIDI helper library
# https://github.com/adafruit/Adafruit_CircuitPython_MIDI


import math
import analogio


class Slider:
    """
    We can connect analog linear potentiometers to act as MIDI continuous controllers
    in a MIDI device. Using the imported Controller class to create a new USB MIDI 
    instrument, we can set up the slider as follows:

    - instrument (for the USB device)
    - pin (for the physical pin connecting the analog slider)
    - command (for the type of MIDI message, typically 0xC0)
    - number (for the control or program message type)
    - calibration (offset to "zero")

    Pin corresponds to the pin number in the board module from CircuitPython.
    RP2040 has 12-bit resolution ADC, but CircuitPython provides 16-bit values.
    """
    def __init__(self, instrument, pin, command=0xC0, number=16, calibration=350):
        self.adc = analogio.AnalogIn(pin)
        self.instrument = instrument
        self.calibration = calibration
        self.command = command
        self.number = number
        self.current_reading = self.adc.value - self.calibration
        self.value = max(min(round(self.current_reading/65535 * 128), 127), 0)

    def set_command(self, command):
        """Provides the type of MIDI meassage (e.g. control, note on...)"""
        self.command = command

    def set_number(self, number):
        """Provides MIDI control or program number if applicable"""
        self.number = number

    def get_update(self):
        """Provides MIDI updates if ADC value changes"""
        # Using simple averaging and taking the max 
        adc_value = self.adc.value - self.calibration
        adc_value = 0.75 * self.current_reading + 0.25 * temadc_valuep_value
        value = max(min(round(adc_value/65535 * 128), 127), 0)

        if value != self.value:
            if self.command == 0x80:
                self.instrument.note_off(value)
            elif self.command == 0x90:
                self.instrument.note_on(value)
            elif self.command == 0xA0:
                self.instrument.pressure(value)
            elif self.command == 0xB0:
                self.instrument.control_change(self.number, value)
            elif self.command == 0xC0:
                self.instrument.program_change(self.number, value)
            elif self.command == 0xD0:
                self.instrument.pressure(value)
            elif self.command == 0xE0:
                self.instrument.pitch_bend(value)
        self.current_reading = adc_value
        self.value = value

    def __repr__(self):
        return '<Slider: pin={}, calibration={}, command={}, control number={}>'.format(
            self.pin, self.calibration, self.command, self.number)