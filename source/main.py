# Expression Controller boot file
# varshaninbox
#
# Requires CircuitPython and MIDI helper library
# https://github.com/adafruit/Adafruit_CircuitPython_MIDI
#
# Contains modified code from Craig Barnes
# https://github.com/cjbarnes18/micropython-midi
#
# The code was modified by introducing Slider class for slider pots,
# and to use CircuitPython instead of MicroPython.
#
# Copyright (C) 2014 Craig Barnes
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import board
import time
from Slider import Slider
from Controller import Controller

if __name__ == '__main__':
    # Create a MIDI USB device
    instrument1 = Controller(channel=1)

    # Connect three analog controllers to the MIDI device
    slider_1 = Slider(instrument1, board.A2, 0xC0, 16)
    slider_2 = Slider(instrument1, board.A0, 0xC0, 17)
    slider_3 = Slider(instrument1, board.A1, 0xC0, 18)

    while True:
        slider_1.get_update()
        slider_2.get_update()
        slider_3.get_update()
        # Update interval is 50 milliseconds
        time.sleep(0.05)
