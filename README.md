# Expression Controller #

MIDI expression controller using RP2040 (Raspberry Pi Pico), CircuitPython and Adafruit's MIDI library. Contains modified code from [Craig Barnes' MIDI device example](https://github.com/cjbarnes18/micropython-midi).

### Installation ###

* Select linear analog potentiometers for your project. I used [987-1832-ND](https://www.digikey.ca/en/products/detail/tt-electronics-bi/PS100-1B1BR10K/7721581). The 3D part included in the `STL` folder can be printed for use with this part.
* Connect linear analog potentiometers to your RP2040 board using common grounds and common voltage. Connect the output pins to analog pins (i.e. `D14-D21`).
* Set your device to use [CircuitPython](https://circuitpython.org/board/raspberry_pi_pico/). Your device should show up as a USB mass storage device.
* Download the [Adafruit MIDI library](https://github.com/adafruit/Adafruit_CircuitPython_MIDI) and copy the `adafruit_midi` folder to your device.
* Copy `main.py`, `Slider.py` and `Controller.py` to your device.
* Test the device in your favourite DAW. Calibrate the sliders by modifying the calibration value in `main.py` (the last initializer argument for `Slider`) to get a zero value. Change update interval if needed.