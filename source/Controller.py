# Expression Controller boot file
# varshaninbox
#
# Requires CircuitPython and MIDI helper library
# https://github.com/adafruit/Adafruit_CircuitPython_MIDI
#
# Contains modified code from Craig Barnes
# https://github.com/cjbarnes18/micropython-midi
#
# The code was modified by introducing Slider class for slider pots,
# and to use CircuitPython instead of MicroPython.
# 
# Copyright (C) 2014 Craig Barnes
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import usb_midi
import adafruit_midi
from adafruit_midi.channel_pressure import ChannelPressure
from adafruit_midi.control_change import ControlChange
from adafruit_midi.note_off import NoteOff
from adafruit_midi.polyphonic_key_pressure import PolyphonicKeyPressure
from adafruit_midi.note_on import NoteOn
from adafruit_midi.pitch_bend import PitchBend
from adafruit_midi.program_change import ProgramChange


class MidiInteger:
    """A midi message sends data as 7 bit values between 0 and 127."""
    def __init__(self, value):
        if 0 <= value < 2 ** 7:
            self.value = value
        else:
            raise ValueError(
                'Invalid midi data value: {}'.format(value),
                'A midi data value must be an integer between 0 and 127')

    def __repr__(self):
        return '<MidiInteger: {}>'.format(self.value)


class BigMidiInteger:
    """Some messages use 14 bit values, these need to be spit down to
    msb and lsb before being sent."""
    def __init__(self, value):
        if 0 <= value <= 2 ** 14:
            self.msb = value // 2 ** 7
            self.lsb = value % 2 ** 7
        else:
            raise ValueError(
                'Invalid midi data value: {}'.format(value),
                'A midi datavalue must be an integer between0'
                ' and {}'.format(2 ** 14))

    def __repr__(self):
        return '<BigMidiInteger: lsb={}, msb={}>'.format(self.lsb, self.msb)


class Controller:
    """A device that is designed to send midi messages to an external
    instrument or sequencer, is commonly referred to as a midi controller.
    http://en.wikipedia.org/wiki/MIDI_controller
    An instance of the Controller class should be considered to be a midi
    contoller in the same context.
    More than one can be created, there are no constraints. but the
    convention is to keep each controller on a separate midi port or channel.
    Usage
    =====
    The following example creates a controller on midi channel one using the
    USB Virtual Com Port device.  Then sends a note on message followed by a
    note off message.
        >>> import pyb
        >>> my_controller = Controller(pyb.USB_VCP(), 1)
        >>> my_controller.note_on(65)
        >>> pyb.delay(100)
        >>> my_controller.note_off(65)
    In order to pass these midi signals on to your computers midi stack, you
    will need to disconnect your console from the Micro Python board,
    and run a serial to midi bridge utility such as ttymidi for Linux.
    Others are available for Mac and Windows.
    With your console still attached you would have seen the characters
    representing the bytes.
    The example above demonstrates the nature of midi note information.
    When a keyboard key is pressed a note_on message is sent.  When the
    key is released a note off message is sent.
    An optional velocity value can also be sent, this usually controls
    the volume of the note played, but this is often used to control other
    characteristics of the sound played.
    """
    COMMANDS = (
        0x80,  # Note Off
        0x90,  # Note On
        0xA0,  # Poly Pressure
        0xB0,  # Control Change
        0xC0,  # Program Change
        0xD0,  # Mono Pressure
        0xE0   # Pich Bend
    )

    def __init__(self, channel=1):
        self.midi = adafruit_midi.MIDI(midi_out=usb_midi.ports[1],
                                       out_channel=channel-1)
        self.channel = channel

    def __repr__(self):
        return '<Controller: port: USB channel: {channel}>'.format(
            channel=self.channel)

    def note_off(self, note, velocity=0):
        """Send a 'Note Off'  message"""
        self.midi.send(NoteOff(note, velocity))

    def note_on(self, note, velocity=127):
        """Send a 'Note On' message"""
        self.midi.send(NoteOn(note, velocity))

    def pressure(self, value, note=None):
        """If a note value is provided then send a polyphonic pressure
        message, otherwise send a Channel (mono) pressure message."""
        if note:
            self.midi.send(PolyphonicKeyPressure(note, value))
        else:
            self.midi.send(ChannelPressure(value))

    def control_change(self, control, value):
        """Send a control e.g. modulation or pedal message."""
        self.midi.send(ControlChange(control, value))

    def program_change(self, value, bank=None):
        """Send a program change message, include bank if provided."""
        self.midi.send(ProgramChange(value))

    def pitch_bend(self, value=0x2000):
        """Send a pich bend message.
        Pich bend is a 14 bit value, centreed at 0x2000"""
        self.midi.send(PitchBend(value))

    def modulation(self, value, fine=False):
        """Send modulation control change."""
        if fine:
            value = MidiInteger(value)
            self.midi.send(ControlChange(33, value.lsb))
            self.midi.send(ControlChange(1, value.msb))
        else:
            self.midi.send(ControlChange(1, value))

    def volume(self, value, fine=False):
        """Send volume control change."""
        if fine:
            value = BigMidiInteger(value)
            self.midi.send(ControlChange(39, value.lsb))
            self.midi.send(ControlChange(7, value.msb))
        else:
            self.midi.send(ControlChange(7, value))

    def all_sound_off(self):
        """Switch all sounds off. """
        self.midi.send(ControlChange(120, 0))

    def reset_all_controllers(self):
        """Set all controllers to their default values."""
        self.midi.send(ControlChange(121, 0))

    def local_control(self, value):
        """Enable or disable local control."""
        if bool(value):
            self.midi.send(ControlChange(122, 127))
        else:
            self.midi.send(ControlChange(122, 0))

    def all_notes_off(self):
        """Send 'All Notes Off' message."""
        self.midi.send(ControlChange(123, 0))

    def panic(self):
        """Reset everything and stop making noise."""
        self.all_sound_off()
        self.reset_all_controllers()
        self.all_notes_off()